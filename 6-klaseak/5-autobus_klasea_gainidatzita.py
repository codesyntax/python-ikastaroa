class Vehicle:
    def __init__(self, name, max_speed, mileage, capacity):
        self.name = name
        self.max_speed = max_speed
        self.mileage = mileage
        self.capacity = capacity

    def vehicle_description(self):
        print("Vehicle Name:", self.name, "Speed:", self.max_speed, "Mileage:", self.mileage)

    def pisu_maximoa(self):
        return self.capacity * 100

class Bus(Vehicle):
    def pisu_maximoa(self):
        #ibilgailuaren_pisua = super().pisu_maximoa()
        ibilgailuaren_pisua = super(Bus, self).pisu_maximoa()
        return ibilgailuaren_pisua * 1.1

School_bus = Bus("School Volvo", 180, 12, 100)
School_bus.vehicle_description()
print(School_bus.pisu_maximoa())