class Vehicle:
    def __init__(self, name, max_speed, mileage, capacity):
        self.name = name
        self.max_speed = max_speed
        self.mileage = mileage
        self.capacity = capacity

    def vehicle_description(self):
        print("Vehicle Name:", self.name, "Speed:", self.max_speed, "Mileage:", self.mileage)

    def pisu_maximoa(self):
        return self.capacity * 100

modelX = Vehicle('ibilgailua', 240, 180, 50)
modelX.vehicle_description()