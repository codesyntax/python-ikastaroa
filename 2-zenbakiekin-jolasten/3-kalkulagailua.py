def kalkulagailua():
    
    print ("(A) batuketa | (B) kenketa | (C) biderketa | (D) zatiketa:")
    op = input("")
    import pdb;pdb.set_trace()
    if op == 'A':
        num1 = int(input("Batuketaren lehen zenbakia: "))
        num2 = int(input("Batuketaren bigarren zenbakia: "))
        result = num1 + num2
    elif op == 'B':
        num1 = int(input("Kenketaren lehen zenbakia: "))
        num2 = int(input("Kenketaren bigarren zenbakia: "))
        result = num1 - num2
    elif op == 'C':
        num1 = int(input("Biderketaren lehen zenbakia: "))
        num2 = int(input("Biderketaren bigarren zenbakia: "))
        result = num1 * num2
    elif op == 'D':
        num1 = int(input("Zatiketaren lehen zenbakia: "))
        num2 = int(input("Zatiketaren bigarren zenbakia: "))
        result = num1 / num2
    else:
        print ("Mesedez A, B, C edo D aukeratu.")
        return kalkulagailua()
    return result

print ("Ongi etorri kalkulagailura. Aukeratu zure eragiketa:")
print(kalkulagailua())

