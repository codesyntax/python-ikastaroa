guztira = 0
erosgaiak = {
    'Tomateak': 1,
    'Piperrak': 2,
    'Saltxitxak': 1.5,
    'Xaboia': 3}
saskia = []
bez = 0
print('Gaur aukeran: Tomateak, Piperrak, Saltxitxak, Xaboia')
erosgaia = input('Zer nahi duzu?')

while erosgaia != '0':
    if erosgaiak.get(erosgaia) is not None:
        saskia.append((erosgaia, erosgaiak[erosgaia]))
        guztira = guztira + erosgaiak[erosgaia]
        bez += erosgaiak[erosgaia] * 0.21
        print('Saskira gehituta')
    else:
        print('Ez daukagu horrelakorik')
    erosgaia = input('Beste zerbait? ')


print(40*'-')
for erositakoa in saskia:
    print('\t{erosgaia}: {prezioa}'.format(erosgaia=erositakoa[0],\
                                            prezioa=erositakoa[1]))

print(40*'-')
print('\tBEZ aurretik:\t{}'.format(guztira))
print('\tBEZ:\t{}'.format(bez))
print('\tGUZTIRA:\t{:.2f}'.format(guztira+bez))
