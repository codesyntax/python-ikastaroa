guztira = 0
erosgaiak = {
    'Tomateak': 1,
    'Piperrak': 2,
    'Saltxitxak': 1.5,
    'Xaboia': 3}
print('Gaur aukeran: {}'.format(', '.join(erosgaiak.keys())))
#print('Gaur aukeran: Tomateak, Piperrak, Saltxitxak, Xaboia')
erosgaia = input('Zer nahi duzu?')

while erosgaia != '0':
    guztira = guztira + erosgaiak[erosgaia]
    print('Saskira gehituta')
    erosgaia = input('Beste zerbait? ')

print('Zure kontua: {} euro dira'.format(guztira))
