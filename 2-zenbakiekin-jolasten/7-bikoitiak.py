def bikoitiak(zenbakiak):
    bikoitien_zerrenda =  []
    for zenb in zenbakiak:
        if zenb % 2 == 0:
            bikoitien_zerrenda.append(zenb)
    return bikoitien_zerrenda

zenbakia = input('Sartu zenbaki bat mesedez')
zenbaki_zerrenda = []
while zenbakia != '0':
    zenbaki_zerrenda.append(int(zenbakia))
    zenbakia = input('Sartu zenbaki bat mesedez')
print(bikoitiak(zenbaki_zerrenda))