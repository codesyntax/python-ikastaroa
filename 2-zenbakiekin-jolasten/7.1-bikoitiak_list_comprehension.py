def bikoitiak(zenbakiak):
    return [x for x in zenbakiak if x % 2 == 0]

zenbakia = input('Sartu zenbaki bat mesedez')
zenbaki_zerrenda = []
while zenbakia != '0':
    zenbaki_zerrenda.append(int(zenbakia))
    zenbakia = input('Sartu zenbaki bat mesedez')
print(bikoitiak(zenbaki_zerrenda))