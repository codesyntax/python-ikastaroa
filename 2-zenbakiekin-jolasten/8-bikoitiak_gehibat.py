def bikoitiak(zenbakiak):
    return list(filter(lambda x: (x % 2 == 0) , zenbakiak))

def bikoitiak_gehibat(zenbakiak):
    bikoitien_lista = bikoitiak(zenbakiak)
    return list(map(lambda x: x + 1 , bikoitien_lista))

zenbakia = input('Sartu zenbaki bat mesedez')
zenbaki_zerrenda = []
while zenbakia != '0':
    zenbaki_zerrenda.append(int(zenbakia))
    zenbakia = input('Sartu zenbaki bat mesedez')
print(bikoitiak_gehibat(zenbaki_zerrenda))