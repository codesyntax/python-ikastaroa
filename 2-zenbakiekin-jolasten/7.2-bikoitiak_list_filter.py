def bikoitiak(zenbakiak):
    return list(filter(lambda x: (x % 2 == 0) , zenbakiak))

zenbakia = input('Sartu zenbaki bat mesedez')
zenbaki_zerrenda = []
while zenbakia != '0':
    zenbaki_zerrenda.append(int(zenbakia))
    zenbakia = input('Sartu zenbaki bat mesedez')
print(bikoitiak(zenbaki_zerrenda))