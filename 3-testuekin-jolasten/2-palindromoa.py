katea =input('Sartu zure hitza')

left = 0
right = len(katea) - 1
palindromoa = True
while right >= left:
    if not katea[left] == katea[right]:
        palindromoa = False
        break
    left += 1
    right -= 1
if palindromoa:
	print('Palindromo da')
else:
	print('Ez da palindromoa')
