katea =input('Sartu zure hitza')

# make it suitable for caseless comparison
katea = katea.casefold()

# reverse the string
rev_str = reversed(katea)

# check if the string is equal to its reverse
if list(katea) == list(rev_str):
   print("Palindromo da.")
else:
   print("Ez da palindromoa.")